<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 5/3/14
 * Time: 9:13 AM
 */

App::uses('AppController', 'Controller');

class ApiController extends AppController {

    public $components = array('Security', 'Auth', 'RequestHandler');

    public function beforeFilter() {

        if(in_array($this->params['action'], array('signup'))){

            $this->Auth->allow();

        } else {

            $this->Auth->authenticate = array(
                'Basic' => array(
                    'userModel' => 'Player',
                    'username' => 'email',
                    'password' => 'password',
                    'realm' => 'api')
            );

            $this->loadModel("Player");

            $email = $_SERVER['PHP_AUTH_USER'];
            $password = $_SERVER['PHP_AUTH_PW'];

            $user = $this->Player->findByEmail($email);

            if ($user && $user['Player']['password'] == Security::hash($password) &&
                $user['Player']['is_active'] == 1) {

                unset($user['Player']['password']);
                $this->set($user);
                $this->Auth->allow($this->params['action']);
            } else {

                if($this->params['action'] == 'check_login') {
                    $this->Auth->allow();
                } else {
                    $this->Security->blackHole($this, 'login');
                }


            }
        }

    }

    public function _api_login($args)
    {
        // Attempt to find a user record using the API key (username) specified.
        $user = $this->Auth->getModel()->findByApiKey($args['username'], array('email', 'password'));
        // If a user is found and the credentials are validated by the AuthComponent::login() method, allow the request.
        if ($user && $this->Auth->login($user)) {
            $this->Auth->allow($this->params['action']);
        } else {
            $this->Security->blackHole($this, 'login');
        }
    }

    public function rankings() {
        /*
         * GET
         * get list of top players ant other stats
         */

        $this->loadModel('Player');

        $top100 = $this->Player->find('all', array(
            'conditions' => array('Player.is_active' => 1),
            'fields' => array('name', 'score'),
            'order' => array('Player.score DESC'),
            'limit' => 100,
            'recursive' => 0
        ));

        $my_position = $this->Player->find('count', array(
            'conditions' => array(
                'Player.score >= ' => intval($this->viewVars['Player']['score']),
                'Player.is_active' => 1
            )
        ));

        $player_count = $this->Player->find('count');

        $result['status'] = 'OK';
        $result['top100'] = $top100;
        $result['my_position'] = $my_position;
        $result['player_count'] = $player_count;
        $result['player'] = $this->viewVars['Player'];

        $this->set(array(
            'result' => $result,
            '_serialize' => array('result')
        ));
    }

    public function competitions() {
        /*
         * GET
         * get list of recent and future competitions
         */

        $this->loadModel('Competition');

        $competitions = $this->Competition->find('all', array(
            'recursive' => 0
        ));

        $result['status'] = 'OK';
        $result['competitions'] = $competitions;

        $this->set(array(
            'result' => $result,
            '_serialize' => array('result')
        ));
    }

    public function competition() {
        /*
         * GET
         * competition details
         * contain participation if any
         * contain groups
         */

        $id = $this->request->query('id');
        $player_id = $this->viewVars['Player']['id'];
        // TODO check

        $this->loadModel('Competition');

        $competition = $this->Competition->findWithParticipationInfo($id, $player_id);

        $result['status'] = 'OK';
        $result['competition'] = $competition;

        $this->set(array(
            'result' => $result,
            '_serialize' => array('result')
        ));
    }

    public function participate() {

        $this->loadModel('Group');
        $this->loadModel('Participation');
        $this->loadModel('Competition');
        $this->loadModel('Voucher');

        $result = array();

        $competition_id = $this->request->query('competition_id');
        $group_id = $this->request->query('group_id');
        $voucher_code = $this->request->query('voucher_code');
        $player_id = $this->viewVars['Player']['id'];

        $group = $this->Group->find('first', array(
            'conditions' => array(
                'Group.id' => $group_id,
                'Group.competition_id' => $competition_id
            ),
            'recursive' => 0
        ));

        if ($group) {
            // Group and competition exist and are associated


            $competition = $this->Competition->findWithParticipationInfo($competition_id, $player_id);

            // TODO check competition availability

            // check participation
            if (isset($competition['PlayerParticipation']['id'])) {

                $result['status'] = 'ERROR';
                $result['message'] = 'Already joined';

            } else {

                $ok = true;

                if(! $competition['Competition']['public']) {
                    // check voucher
                    if (isset($voucher_code)) {

                        $voucher_exist = $this->Voucher->hasAny(array(
                                'competition_id' => $competition_id,
                                'LOWER(code)' => strtolower($voucher_code),
                                'claimed' => false
                            )
                        );

                        if($voucher_exist) {

                            // claim voucher
                            $this->Voucher->updateAll(
                                array('Voucher.claimed' => true),
                                array('Voucher.code' => strtoupper($voucher_code))
                            );

                        } else {

                            $ok = false;

                            $result['status'] = 'ERROR';
                            $result['message'] = 'Incorrect voucher code. Maybe already used';
                        }

                    } else {
                        $result['status'] = 'ERROR';
                        $result['message'] = 'Competition is not public. Voucher code needed';
                    }
                }

                // Public OR ( Private AND correct voucher)
                if ($ok) {
                    // participate
                    $this->Participation->create();
                    $this->Participation->set(array(
                        'competition_id' => $competition_id,
                        'group_id' => $group_id,
                        'player_id' => $player_id
                    ));

                    if ($this->Participation->save()) {
                        $result['status'] = 'OK';
                        $result['message'] = 'Joined successfully';
                    } else {
                        $result['status'] = 'ERROR';
                        $result['message'] = 'Error during joining competition';
                    }

                }

            }

        } else {
            $result['status'] = 'ERROR';
            $result['message'] = 'Incorrect group and competition IDs';
        }

        $this->set(array(
            'result' => $result,
            '_serialize' => array('result')
        ));
    }

    public function new_game() {

        $this->loadModel('Participation');
        $this->loadModel('Competition');
        $this->loadModel('Question');
        $this->loadModel('Game');

        $MAX_QUESTIONS = 15;

        $participation_id = $this->request->query('participation_id');
        $player_id = $this->viewVars['Player']['id'];

        $result = array();
        $competition_id = null;
        $group_id = null;

        $ok = true;
        $questions_text = null;

        if (isset($participation_id) ) {
            // is a cometition

            $participation = $this->Participation->findById($participation_id);

            if($participation) {

                $competition_id = $participation['Participation']['competition_id'];
                $group_id = $participation['Participation']['group_id'];

                $competition = $this->Competition->find('first', array(
                    'conditions' => array('Competition.id' => $competition_id)
                ));

                if ($competition['Competition']['active'] == 1) {

                    if ($participation['Participation']['started']) {

                        $ok = false;
                        $result['status'] = 'ERROR';
                        $result['message'] = 'Game already created';

                    } else {

                        // get question id-s

                        $questions = $this->Question->find('list', array(
                            'fields' => array('id'),
                            'conditions' => array(
                                'Question.competition_id' => $competition_id
                            )
                        ));
                    }

                } else {
                    $ok = false;
                    $result['status'] = 'ERROR';
                    $result['message'] = 'Game not started';
                }




            } else {
                $ok = false;
                $result['status'] = 'ERROR';
                $result['message'] = 'Incorrect participation ID';
            }

        } else {
            // is ordinary game

            $questions = $this->Question->find('list', array(
                'fields' => array('Question.id'),
                'joins' => array(
                    array(
                        'table' => 'answers',
                        'alias' => 'Answer',
                        'type' => 'left',
                        'conditions'=> "`Answer`.`question_id` = `Question`.`id` AND `Answer`.`player_id` = `$player_id`"
                    )
                ),
                'group' => array('Question.id'),
                'order' => array('IFNULL(COUNT(Answer.id), 0) ASC', 'RAND()'),
                'limit' => $MAX_QUESTIONS,
                'conditions' => array(
                    'confirmed' => true,
                    'Question.competition_id IS NULL'
                )
            ));

        }


        if($ok) {

            $questions_text = implode(",", $questions);

            $this->Game->read(null, 1);
            $this->Game->set(array(
                'competition_id' => $competition_id,
                'participation_id' => $participation_id,
                'player_id' => $player_id,
                'group_id' => $group_id,
                'questions' => $questions_text
            ));

            $game = $this->Game->save();

            if($game) {

                $result['status'] = 'OK';
                $result['message'] = 'Game created successfully';
                $result['game'] = $game;
            }

            // TODO create game

            if (isset($competition_id)) {
                $this->Participation->updateAll(
                    array('Participation.started' => true),
                    array('Participation.id' => $participation_id)
                );
            }

        }

        $this->set(array(
            'result' => $result,
            '_serialize' => array('result')
        ));

    }

    public function answer() {
        // TODO post

        $this->loadModel('Question');
        $this->loadModel('Answer');
        $this->loadModel('Game');
        $this->loadModel('Player');

        $game_id = $this->request->query('game_id');
        $question_number = $this->request->query('question_number');
        $answer = $this->request->query('answer');
        $player_id = $this->viewVars['Player']['id'];

        $is_competition = false;

        $result = array();

        $game = $this->Game->find('first', array(
            'conditions' => array(
                'Game.id' => $game_id,
                'Game.player_id' => $player_id,
                'Game.current_question' => $question_number,
                'Game.endDate' => null
            ),
            'recursive' => 1
        ));

        if($game) {

            $questions_ids = explode(',',$game['Game']['questions']);

            if ($question_number == 0) {
                // user is requesting first question without answering any


                $next_question_id = $questions_ids[0];
                $next_question = $this->Question->find('first', array(
                    'conditions' => array('Question.id' => $next_question_id),
                    'fields' => array(
                        'Question.question',
                        'Question.option1',
                        'Question.option2',
                        'Question.option3',
                        'Question.option4',
                    )
                ));

                $this->Game->id = $game_id;

                // update currect question number
                $this->Game->saveField('current_question', $question_number +1);

                $result['status'] = 'OK';
                $result['message'] = 'Got question';
                $result['next'] = $next_question;

            } else {
                // user is answering a question

                $is_active = true;

                // is competition?
                $is_competition = isset($game['Game']['participation_id']);

                if ($is_competition) {
                    $is_active = $game['Competition']['active'];
                }

                $is_last_question = false;
                $answer_correct = false;

                if ($is_active) {

                    // get current question
                    $question_id = $questions_ids[$question_number-1];

                    // check answer
                    $answer_correct = $this->Question->hasAny(array(
                        'Question.id' => $question_id,
                        'Question.correct_option' => $answer
                    ));


                    if (!$is_competition) {
                        $result['correct'] = $answer_correct;
                    }

                    $is_last_question = $question_number == count($questions_ids);

                    // save answer
                    $this->Answer->read(null, 1);
                    $this->Answer->set(array(
                        'question_id' => $question_id,
                        'player_id' => $player_id,
                        'answer' => $answer,
                        'game_id' => $game_id
                    ));


                    if ($this->Answer->save()) {
                        $result['status'] = 'OK';
                        $result['message'] = 'Answer Saved';
                    } else {
                        $result['message'] = 'Answer could not Saved';
                    }

                    $this->Game->id = $game_id;

                    $game_score = null;

                    if($is_competition) {

                        if($answer_correct) {
                            $this->Game->saveField('score', $game['Game']['score'] + 100);
                        }

                    } else {

                        $result['last_correct'] = $answer_correct;

                        if ($answer_correct) {

                            // get question's answer count
                            $answer_count = $this->Answer->find('count', array(
                                'conditions' => array(
                                    'Answer.player_id' => $player_id,
                                    'Answer.question_id' => $question_id
                                )
                            ));

                            // prevent error if answer could not save
                            if($answer_count == 0) $answer_count = 1;

                            $added_score = round(100/$answer_count);
                            $game_score = $game['Game']['score'] + $added_score;

                            $this->Game->saveField('score', $game_score);

                            $result['game_score'] = $game_score;
                            $result['added_score'] = $added_score;

                        } else {

                            $game_score = $game['Game']['score'] ;
                            $result['game_score'] = $game_score;
                        }

                    }

                    // update currect question number
                    $this->Game->saveField('current_question', $question_number +1);


                } else {
                    // time is up
                }

                if($is_last_question || !$is_active ||
                    (!$is_competition && !$answer_correct) ) {

                    // end game
                    $this->Game->id = $game_id;
                    $this->Game->saveField('endDate', date('Y-m-d H:i:s'));

                    $result['status'] = "OK";
                    $result['ended'] = true;

                    if (! $is_competition) {

                        // save total player score
                        $this->Player->updateAll(
                            array('Player.score' => 'Player.score + '.$game_score),
                            array('Player.id' => $player_id)
                        );

                        $player = $this->Player->find('first', array(
                            'conditions' => array('Player.id' => $player_id),
                            'recursive' => 0
                        ));

                        $result['total_score'] = $player['Player']['score'];


                    }
                } else {

                    $next_question_id = $questions_ids[$question_number];
                    $next_question = $this->Question->find('first', array(
                        'conditions' => array('Question.id' => $next_question_id),
                        'fields' => array(
                            'Question.question',
                            'Question.option1',
                            'Question.option2',
                            'Question.option3',
                            'Question.option4',
                        )
                    ));

                    $result['next'] = $next_question;
                }
            }



        } else {

            $result['status'] = 'ERROR';
            $result['message'] = 'Game incorrect';
        }




        $this->set(array(
            'result' => $result,
            '_serialize' => array('result')
        ));
    }

    public function signup() {
        // TODO post
        $this->loadModel('Player');

        $firstName = $this->request->query('first_name');
        $lastName = $this->request->query('last_name');
        $email = $this->request->query('email');
        $phone = $this->request->query('phone');
        $password = $this->request->query('password');

        $this->Player->create();
        $this->Player->set(array(
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email,
            'phone' => $phone,
            'password' => Security::hash($password)
        ));

        $success = $this->Player->save();

        $result = array();

        if($success) {

            $result['status'] = 'OK';
            $result['message'] = 'Signed up';
            $result['player'] = $success['Player'];

        } else {
            $result['status'] = 'ERROR';
            $result['message'] = 'Could not sign up';
        }

        $this->set(array(
            'result' => $result,
            '_serialize' => array('result')
        ));

    }

    public function check_login() {

        $result = array();

        if(isset($this->viewVars['Player'])) {
            $result['status'] = 'OK';
            $result['message'] = 'Authenticated';
            $result['user'] = $this->viewVars['Player'];
        } else {
            $result['status'] = 'ERROR';
            $result['message'] = 'Not Authenticated';
        }




        $this->set(array(
            'result' => $result,
            '_serialize' => array('result')
        ));
    }



}
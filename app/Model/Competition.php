<?php
App::uses('AppModel', 'Model');
/**
 * Competition Model
 *
 * @property Group $Group
 * @property Question $Question
 */
class Competition extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

    public $virtualFields = array(
        'active' => 'NOW() BETWEEN start_date AND end_date',
        'ended' => 'NOW() > end_date'
    );


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Group' => array(
			'className' => 'Group',
			'foreignKey' => 'competition_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Question' => array(
			'className' => 'Question',
			'foreignKey' => 'competition_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
        'Participation' => array(
            'className' => 'Participation',
            'foreignKey' => 'competition_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
	);

    public function findWithParticipationInfo($id = null, $player_id = null) {

        $this->unbindModel(
            array('hasMany' => array('Question', 'Participation'))
        );

        $this->bindModel(
            array('hasOne' => array(
                'PlayerParticipation' => array(
                    'className' => 'Participation',
                    'conditions' => 'player_id = `'.$player_id.'`'
                )
            ))
        );

        return $this->find('first', array(
            'conditions' => array('Competition.id' => $id),
            'recursive' => 1
        ));
    }

}

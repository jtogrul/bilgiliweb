<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 5/4/14
 * Time: 3:01 PM
 */

class VoucherShell extends AppShell {

    public $uses = array('Voucher');

    public function main() {
        $this->out('Use: cake voucher generate <competition_id> <number_of_vouchers>');
    }

    public function generate() {

        $competition_id = $this->args[0];
        $number = $this->args[1];

        for ($i = 0; $i < $number; $i++) {

            $_code = strtoupper(md5(uniqid(rand(),true)));
            $code = substr($_code, 0, 6);

            //$code = md5(uniqid($competition_id, true));
            $this->out($i . ' Code: ' . $code);

            $this->Voucher->create();
            $saved = $this->Voucher->save(array(
                'competition_id' => $competition_id,
                'code' => $code
            ));

            if (! $saved) {
                $number++;
            }

        }

    }

    public function remove() {

        $competition_id = $this->args[0];

        $success = $this->Voucher->deleteAll(array('Voucher.competition_id' => $competition_id), false);

        $this->out('OK?: ' . $success);

    }

} 